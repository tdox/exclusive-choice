{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- {-# OPTIONS_GHC -Wall #-}

module CLI where

import Core
import Util

-- base
import Prelude hiding (putStr, putStrLn, getLine)

import Control.Monad (void, when)
import Data.Either (isLeft)
import System.Exit (exitFailure)
import System.IO (hFlush, stdout)

-- containers
import Data.Map ((!), fromList)

-- text
import Data.Text (Text, intercalate, pack)
import qualified Data.Text as T
import Data.Text.IO (getLine, putStr, putStrLn)

--------------------------------------------------------------------------------


main1 :: IO ()
main1 = do
  let
    s0 = State 0 []


    {-
    exclsRules =  [ ("AIRCRAFT", ["AIRCRAFT"])
                  , ("ORIGIN", ["ORIGIN", "LAYOVER"])
                  , ("DESTINATION", ["DESTINATION", "LAYOVER"])
                  , ("LAYOVER", ["ORIGIN", "DESTINATION", "LAYOVER"])
                  ]
-}

    inclGroups = fromList [ ("AIRCRAFT", ["AIRCRAFT", "AIRCRAFT_SIZE", "AIRCRAFT_TAIL"])
                          , ("ORIGIN", ["ORIGIN", "ORIGIN_COUNTRY"])
                          , ("DESTINATION", ["DESTINATION", "DESTINATION_COUNTRY"])
                          , ("LAYOVER", ["LAYOVER", "LAYOVER_COUNTRY", "LAYOVER_HOMEBASE"])
                          , ("LEG", ["LEG_TYPE", "LEG_TIME"])
                          , ("TRIP", ["TRIP_TYPE"])
                          , ("DEPARTURE", ["DEPARTURE_DATE_EFFECTIVE"])
                          , ("ARRIVAL", ["ARRIVAL_DATE_EFFECTIVE"])
                          , ("PROGRAM", ["PROGRAM"])
                          ]


    -- TODO derive excls from exclsRules and inclGroups
    excls = fromList [ ("AIRCRAFT",      inclGroups ! "AIRCRAFT")
                     , ("AIRCRAFT_SIZE", inclGroups ! "AIRCRAFT")
                     , ("AIRCRAFT_TAIL", inclGroups ! "AIRCRAFT")
                     , ("ORIGIN",         (inclGroups ! "ORIGIN") <> (inclGroups ! "LAYOVER"))
                     , ("ORIGIN_COUNTRY", (inclGroups ! "ORIGIN") <> (inclGroups ! "LAYOVER"))
                     , ("DESTINATION",         (inclGroups ! "DESTINATION") <> (inclGroups ! "LAYOVER"))
                     , ("DESTINATION_COUNTRY", (inclGroups ! "DESTINATION") <> (inclGroups ! "LAYOVER"))
                     , ("LAYOVER",          (inclGroups ! "ORIGIN") <> (inclGroups ! "DESTINATION") <> (inclGroups ! "LAYOVER"))
                     , ("LAYOVER_COUNTRY",  (inclGroups ! "ORIGIN") <> (inclGroups ! "DESTINATION") <> (inclGroups ! "LAYOVER"))
                     , ("LAYOVER_HOMEBASE", (inclGroups ! "ORIGIN") <> (inclGroups ! "DESTINATION") <> (inclGroups ! "LAYOVER"))
                     ]


  let eValid = isValid excls

  when (isLeft eValid) $ do
    let Left err = eValid
    putStrLn err
    exitFailure

  void $ go excls s0
 

go :: Exclusions -> State -> IO State
go excls s0 = do
  putStrLn $ prettyPrintState excls s0
  cmd <- getNextCommandWithState excls s0

  case cmd of
    CreateMenu -> do
      s1 <- doCreateMenu excls s0
      s2 <- doSelectItemFromMenu excls s1 (sNumMenus s1)
      go excls s2
      
    SelectItem -> do
      s1 <- doSelectItem excls s0
      go excls s1
      
    DeleteMenu -> do
      s1 <- doDeleteMenu s0
      go excls s1


getNextCommandWithState :: Exclusions -> State -> IO Command
getNextCommandWithState excls s = do
  let possCmds = getPossibleCmds excls s
      nCmds = length possCmds

  putStr $ "select a command [" <> mkOptions (map (pack . show) possCmds)
            <> "] : "

  hFlush stdout
  iCmd <- getNumberCli nCmds
  return $ possCmds !! (iCmd - 1)


doCreateMenu :: Exclusions -> State -> IO State
doCreateMenu excls s0 = do
  let s1 = createMenu s0
  putStr $ prettyPrintState excls s1
  return s1


doSelectItem :: Exclusions -> State -> IO State
doSelectItem excls s0 = do
  iMenu <- promptForMenu s0
  doSelectItemFromMenu excls s0 iMenu

doSelectItemFromMenu :: Exclusions -> State -> Int -> IO State
doSelectItemFromMenu excls s0 iMenu = do
  let
    menus = mkAllMenus excls s0 :: [Menu]
    menu = menus !! (iMenu - 1) :: Menu
    menuItems = mItems menu :: [Item]
    nMenuItems = length menuItems

  putStr $ "select an item (between 1 and "
    <> (pack (show nMenuItems)) <> "): "
  hFlush stdout
     
  iItem <- getNumberCli $ length menuItems

  return $ selectItemFromMenu excls s0 (iMenu - 1) (iItem - 1)


promptForMenu :: State -> IO Int
promptForMenu s0 = do
  let nMenus = sNumMenus s0
  putStr $ "select a menu (between 1 and " <> (pack (show nMenus)) <> "): "
  hFlush stdout
  getNumberCli nMenus


doDeleteMenu :: State -> IO State
doDeleteMenu s0 = do
  iMenu <- promptForMenu s0
  return $ deleteMenu (iMenu - 1) s0


menuText ::
     Exclusions
  -> Choices -- ^ previously selected items (from other menus)
  -> Item    -- ^ item selected on this menu
  -> Text    -- ^ textual representation of the menu
  
menuText excls prevSelected item =
  prettyPrintMenu $ mkMenu0 excls prevSelected item
        


mkPointer :: Maybe Int -> Int -> Text
mkPointer mItemIndex currIndex =
  case mItemIndex of
    Just itemIndex ->
      if itemIndex == currIndex then "> " else "  "
    _ -> "  "


prettyPrintMenu :: Menu -> Text
prettyPrintMenu (Menu menuItems itemIndex) = menu
  where
    pointers = map (\currIndex -> mkPointer itemIndex currIndex) [0..]
    menuItemsTxt = zipWith (<>) pointers menuItems :: [Text]
    menu = mconcat $ zipWith (\t i ->  t <> " (" <> (pack (show i)) <> ")\n")
                               menuItemsTxt
                               [1 ..]


prettyPrintState :: Exclusions -> State -> Text
prettyPrintState excls s@State{..} = heading <> allMenusTxt
  where
    allMenus = mkAllMenus excls s
    allMenusTxt0 = map prettyPrintMenu allMenus
    heading = "\n\nMenus\n"
    
    allMenusTxt =
      mconcat $ zipWith (\m i -> mconcat ["\nmenu ", pack (show i), "\n", m])
                        allMenusTxt0
                        [1..]


mkOptions :: [Text] -> Text
mkOptions xs = intercalate ", " $ zipWith combine xs [1..]

combine :: Text -> Int -> Text
combine t0 i = t0 <> " (" <> pack (show i) <> ")"


getNumberCli :: Int -> IO Int
getNumberCli maxN = do
  ln <- getLine
  
  if (T.null ln || not (allDigits ln))
    then do
      numberPrompt maxN
      getNumberCli maxN
    else do
  
      let
        i = read $ T.unpack ln :: Int

      if (i < 1 || i > maxN)
      then do
        numberPrompt maxN
        getNumberCli maxN
      else
        pure i

    

numberPrompt :: Int -> IO ()
numberPrompt maxN = do
  putStr $ "enter a number between 1 and " <> (pack (show maxN))
           <> ", inclusive: "
    
  hFlush stdout
