{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wall #-}

module Lib where

import Core
import CLI
import Util

-- base
import Prelude hiding (putStr, putStrLn, getLine)
import Data.List ((\\))
import Control.Monad (when)
import Data.Either (isLeft)
import System.Exit (exitFailure, exitSuccess)

-- containers
import Data.Map ((!), fromList, toList)

-- text
import Data.Text (Text, intercalate, pack)
import qualified Data.Text as T
import Data.Text.IO (getLine, putStrLn)

--------------------------------------------------------------------------------




selectItem :: Exclusions -> Item -> [Item] -> Either Text [Item]
selectItem excls x xs0 =
  if not (x `elem` xs0)
  then Left (x <> " is not in xs0")
  else Right (xs0 \\ (excls ! x))



iteration :: Exclusions -> [Item] -> IO ()
iteration excls choices0 = do
  when (null choices0) $ exitSuccess
  
  let choices1 = mkOptions choices0
  
  putStrLn $ "choose from " <> choices1
  ln <- getLine

  when (not (allDigits ln)) $ iteration excls choices0
  
  let
    i = read $ T.unpack ln :: Int
    n = length choices0

  when (i < 1 || i > n) $ do
    putStrLn $ "enter a number between 1 and " <> (pack (show n))
              <> ", inclusive"
      
    iteration excls choices0

  let choice = choices0 !! (i-1)
    
  putStrLn $ "you chose " <> pack (show i) <> ": " <> choice
  let Right choices2 = selectItem excls choice choices0
  iteration excls choices2

mkMenuTxt ::
     Exclusions
  -> Choices -- ^ previously selected items (from all menus, including this)
  -> Item -- ^ item selected on this menu
  -> Text

mkMenuTxt excls prevSelected item =
  prettyPrintMenu $ mkMenu excls prevSelected item



mkMenusTxt ::
     Exclusions
  -> Choices -- ^ previously selected items (from all menus)
  -> [Text]  -- ^ the menus (each one has an item previously selected)

mkMenusTxt excls prevSelected =
  map prettyPrintMenu $ mkMenus excls prevSelected



mkFirstMenu :: Exclusions -> Text
mkFirstMenu excls = menu
  where
    items = getChoices excls
    menu = mconcat $ map (\t -> T.snoc t '\n') items

-- mkLists [1, 2, 3] == [[1], [1, 2], [1, 2, 3]]
mkLists :: [a] -> [[a]]
mkLists xs = map (\i -> take i xs) [1 .. length xs]

  

main1 :: IO ()
main1 = do
  
  let
    excls = fromList [ ("AIRCRAFT", ["AIRCRAFT"])
                     , ("ORIGIN", ["ORIGIN", "LAYOVER"])
                     , ("DESTINATION", ["DESTINATION", "LAYOVER"])
                     , ("LAYOVER", ["ORIGIN", "DESTINATION", "LAYOVER"])
                     ]

  putStrLn $ prettyPrintExclusions excls
  putStrLn ""
            
  let eValid = isValid excls

  when (isLeft eValid) $ do
    let Left err = eValid
    putStrLn err
    exitFailure

  iteration excls $ getChoices excls


prettyPrintTuple :: (Show a, Show b) => (a, b) -> Text
prettyPrintTuple (x, y) =
  pack (show x) <> ": " <> pack (show y)

prettyPrintExclusions :: Exclusions -> Text
prettyPrintExclusions xs = intercalate "\n" $ map prettyPrintTuple $ toList xs





--------------------------------------------------------------------------------
