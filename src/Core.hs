{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wall #-}

module Core where

import Util

-- base
import Prelude hiding (putStr, putStrLn, getLine)

import Control.Monad (when)
import Data.List ((\\), elemIndex)
import Data.Either (lefts, rights)

-- containers
import Data.Map (Map, (!), toList)
import qualified Data.Map as M

-- errors
import Control.Error.Util (note)

-- text
import Data.Text (Text)

--------------------------------------------------------------------------------

type Item = Text
type Exclusions = Map Item [Item]
type Choices = [Item]


data State = State
  { sNumMenus :: Int
  , sSelected :: [Item]
  } deriving Show


data Menu = Menu
  { mItems     :: [Item]
  , mSelected  :: Maybe Int
  }

data Command = CreateMenu | DeleteMenu | SelectItem deriving Show


--------------------------------------------------------------------------------


isValid :: Exclusions -> Either Text Bool
isValid excls = do
  let
    pairs0 = concatMap mkPairs $ toList excls :: [(Item, Item)]
    pairs = addSwaps pairs0
    eRecipFounds = map (recipFound excls) pairs :: [Either Text Bool]
    lfts = lefts eRecipFounds

  when (not (null lfts)) $ Left $ head lfts

  if not (null lfts)
  then Left $ head lfts
  else do
    
    let allTrue = and $ rights eRecipFounds

    if allTrue
    then Right True
    else Left "could not find all vals"


recipFound :: Exclusions -> (Item, Item) -> Either Text Bool
recipFound excls (_, val) = do
  valVals :: [Item]  <-
    note ("could not find item " <> val) $ M.lookup val excls

  if val `elem` valVals
  then Right True
  else Left ("could not find " <> val)
         --  <> pack " in " <> (map T.pack valVals))


getChoices :: Exclusions -> Choices
getChoices excls = map fst $ toList excls

createMenu :: State -> State
createMenu s0 = s0 {sNumMenus = sNumMenus s0 + 1}


mkMenu0 ::
     Exclusions
  -> Choices -- ^ previously selected items (from other menus)
  -> Item    -- ^ item selected on this menu
  -> Menu

mkMenu0  excls prevSelected item = Menu menuItems $ Just itemIndex
  where
    menuItems = computeRemainingChoices excls prevSelected
    Just itemIndex = elemIndex item menuItems
  

mkMenus ::
     Exclusions
  -> Choices -- ^ previously selected items (from all menus)
  -> [Menu]  -- ^ the menus (each one has an item previously selected)

mkMenus excls prevSelected = map (mkMenu excls prevSelected) prevSelected

mkAllMenus :: Exclusions -> State -> [Menu]
mkAllMenus excls  State{..} = allMenus
  where
    menusWithSelects = mkMenus excls sSelected :: [Menu]
    remainingChoices = computeRemainingChoices excls sSelected
    newMenu = Menu remainingChoices Nothing

    allMenus = if sNumMenus > length sSelected
               then menusWithSelects <> [newMenu]
               else menusWithSelects


mkMenu ::
     Exclusions
  -> Choices -- ^ previously selected items (from all menus, including this)
  -> Item -- ^ item selected on this menu
  -> Menu

mkMenu excls prevSelected item = mkMenu0 excls otherItems item
  where otherItems = getOtherItems prevSelected item


getPossibleCmds :: Exclusions -> State -> [Command]
getPossibleCmds excls s =
  filter (commandIsPossible excls s) [CreateMenu, DeleteMenu, SelectItem]

commandIsPossible :: Exclusions -> State -> Command -> Bool
commandIsPossible excls s cmd =
  case cmd of
    CreateMenu -> createCmdIsPossible excls s
    DeleteMenu -> deleteCmdIsPossible s
    SelectItem -> selectItemCmdIsPossible s


createCmdIsPossible :: Exclusions -> State -> Bool
createCmdIsPossible excls s = length remainingChoices > 0
  where remainingChoices = computeRemainingChoices excls $ sSelected s

deleteCmdIsPossible :: State -> Bool
deleteCmdIsPossible s = sNumMenus s > 0

selectItemCmdIsPossible :: State -> Bool
selectItemCmdIsPossible s = sNumMenus s > 0


computeRemainingChoices ::
     Exclusions
  -> Choices -- ^ items selected so far
  -> Choices -- ^ remaining items that may be selected

computeRemainingChoices excls selected = allChoices \\ allExcluded
  where
    allChoices = getChoices excls
    allExcluded = concatMap (excls !) selected :: [Item]


deleteMenu :: Int -> State -> State
deleteMenu iMenu (State numMenus0 selected0) = 
  State (numMenus0 - 1) $ removeElement selected0 iMenu


selectItemFromMenu :: Exclusions -> State -> Int -> Int -> State
selectItemFromMenu excls s0 iMenu iItem = s1
  where
    nMenus = sNumMenus s0
    selected = sSelected s0 :: [Item]
    menus = mkAllMenus excls s0 :: [Menu]
    menu = menus !! iMenu :: Menu
    menuItems = mItems menu :: [Item]
    selectedItem = menuItems !! iItem
    selectingFromNewMenu = nMenus > length (sSelected s0)
  
    selected1 = if selectingFromNewMenu
                then selected <> [selectedItem]
                else updateList selected iMenu selectedItem

    s1 = State nMenus selected1

