{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wall #-}

module Util where

-- base
import Prelude hiding (putStr, putStrLn, getLine)
import Data.Char (isDigit)
import Data.List ((\\))
import Data.Tuple (swap)

-- text
import Data.Text (Text, unpack)

--------------------------------------------------------------------------------

  
updateList :: [a] -> Int -> a -> [a]

updateList [] _ _ = []

updateList xs0 i x =
  if i < 0 || i >= length xs0
  then xs0
  else if i == 0
       then x : tail xs0
       else
         let (ys, (_:zs)) = splitAt i xs0
         in ys ++ x:zs

removeElement :: [a] -> Int -> [a]

removeElement [] _ = []

removeElement xs0 i =
  if i < 0 || i >= length xs0
  then xs0
  else if i == 0
       then tail xs0
       else
         let (ys, (_:zs)) = splitAt i xs0
         in ys ++ zs
         

allDigits :: Text -> Bool
allDigits xs = and $ map isDigit $ unpack xs


getOtherItems ::
     Eq a 
  => [a] -- ^ entire list
  -> a   -- ^ an item in the list
  -> [a] -- ^ the other items in the list

getOtherItems xs x = xs \\ [x]


mkPairs :: (a, [b]) -> [(a, b)]
mkPairs (key, vals) = zip (repeat key) vals


addSwaps :: [(a, a)] -> [(a, a)]
addSwaps xs = xs ++ (map swap xs)
