{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# OPTIONS_GHC -Wall #-}

module Test where

import Core
import CLI
import Util
import Lib

-- base
import Prelude hiding (putStr, putStrLn, getLine)

-- containers
import Data.Map (fromList)

-- text
import Data.Text (Text)


-- tests

excls1 :: Exclusions
excls1 = fromList [ ("AIRCRAFT", ["AIRCRAFT"])
                  , ("ORIGIN", ["ORIGIN", "LAYOVER"])
                  , ("DESTINATION", ["DESTINATION", "LAYOVER"])
                  , ("LAYOVER", ["ORIGIN", "DESTINATION", "LAYOVER"])
                  ]

ok1, ok2, ok3, ok4, ok5, ok6, ok7, ok8, ok9, ok10, ok11, ok12
   , ok13, ok14, ok15, ok16, ok17, ok18, ok19, ok20, ok21, ok22
   , ok23, ok24, ok25, ok26 :: Bool

menu1 :: Text
menu1 = menuText excls1 ["LAYOVER"] "AIRCRAFT"
ok1 = menu1 == "> AIRCRAFT (1)\n"

menu2 :: Text
menu2 = menuText excls1 [] "AIRCRAFT"
ok2 =
  menu2 == "> AIRCRAFT (1)\n  DESTINATION (2)\n  LAYOVER (3)\n  ORIGIN (4)\n"

menu3 :: Text
menu3 = menuText excls1 ["ORIGIN"] "AIRCRAFT"
ok3 = menu3 == "> AIRCRAFT (1)\n  DESTINATION (2)\n"

menu4 :: Text
menu4 = menuText excls1 ["AIRCRAFT"] "LAYOVER"
ok4 = menu4 == "  DESTINATION (1)\n> LAYOVER (2)\n  ORIGIN (3)\n"

menu5 :: Text
menu5 = menuText excls1 ["AIRCRAFT", "ORIGIN"] "DESTINATION"
ok5 = menu5 == "> DESTINATION (1)\n"

menu6 :: Text
menu6 = mkMenuTxt excls1 ["AIRCRAFT"] "AIRCRAFT"
ok6 =
  menu6 == "> AIRCRAFT (1)\n  DESTINATION (2)\n  LAYOVER (3)\n  ORIGIN (4)\n"

menu7 :: Text
menu7 = mkMenuTxt excls1 ["ORIGIN", "AIRCRAFT"] "AIRCRAFT"
ok7 = menu7 == "> AIRCRAFT (1)\n  DESTINATION (2)\n"

menu8 :: Text
menu8 = mkMenuTxt excls1 ["LAYOVER", "AIRCRAFT"] "AIRCRAFT"
ok8 = menu8 == "> AIRCRAFT (1)\n"

menu9 :: Text
menu9 = mkMenuTxt excls1 ["ORIGIN", "DESTINATION"] "DESTINATION"
ok9 = menu9 == "  AIRCRAFT (1)\n> DESTINATION (2)\n"

menus1 :: [Text]
menus1 = mkMenusTxt excls1 ["AIRCRAFT"]
ok10 =
  menus1 == ["> AIRCRAFT (1)\n  DESTINATION (2)\n  LAYOVER (3)\n  ORIGIN (4)\n"]

menus2 :: [Text]
menus2 = mkMenusTxt excls1 ["AIRCRAFT", "ORIGIN"]
ok11 = menus2 == ["> AIRCRAFT (1)\n  DESTINATION (2)\n"
                 ,"  DESTINATION (1)\n  LAYOVER (2)\n> ORIGIN (3)\n"
                 ]

menus3 :: [Text]
menus3 = mkMenusTxt excls1 ["AIRCRAFT", "ORIGIN", "DESTINATION"]
ok12 = menus3 == ["> AIRCRAFT (1)\n"
                 ,"> ORIGIN (1)\n"
                 ,"> DESTINATION (1)\n"
                 ]

menus4 :: [Text]
menus4 = mkMenusTxt excls1 ["LAYOVER"]
ok13 =
  menus4 == ["  AIRCRAFT (1)\n  DESTINATION (2)\n> LAYOVER (3)\n  ORIGIN (4)\n"]

menus5 :: [Text]
menus5 = mkMenusTxt excls1 ["LAYOVER", "AIRCRAFT"]
ok14 = menus5 == [ "  DESTINATION (1)\n> LAYOVER (2)\n  ORIGIN (3)\n"
                 , "> AIRCRAFT (1)\n"
                 ]

state1 :: State
state1 = State 1 []

ppState1, ppState2, ppState3 :: Text
ppState1 = prettyPrintState excls1 state1
ok15 = ppState1 == "\n\nMenus\n\nmenu 1\n  AIRCRAFT (1)\n  DESTINATION (2)\n  LAYOVER (3)\n  ORIGIN (4)\n"


state2 :: State
state2 = State 1 ["AIRCRAFT"]
ppState2 = prettyPrintState excls1 state2
ok16 = ppState2 ==
  "\n\nMenus\n\nmenu 1\n> AIRCRAFT (1)\n  DESTINATION (2)\n  LAYOVER (3)\n  ORIGIN (4)\n"

state3 :: State
state3 = State 2 ["AIRCRAFT"]
ppState3 = prettyPrintState excls1 state3
ok17 = ppState3 ==
  "\n\nMenus\n\nmenu 1\n> AIRCRAFT (1)\n  DESTINATION (2)\n  LAYOVER (3)\n  ORIGIN (4)"
  <> "\n\nmenu 2\n  DESTINATION (1)\n  LAYOVER (2)\n  ORIGIN (3)\n"

xs0, xs1, xs2, xs3, xs4, xs5, xs6, xs7, xs8, xs9 :: [Int]
xs0 = [1, 2, 3, 4]

xs1 = updateList xs0 0 5
ok18 = xs1 == [5, 2, 3, 4]

xs2 = updateList xs0 2 5
ok19 = xs2 == [1, 2, 5, 4]

xs3 = updateList xs0 9 5
ok20 = xs3 == xs0

xs4 = updateList xs0 3 5
ok21 = xs4 == [1, 2, 3, 5]

xs5 = updateList xs0 4 5
ok22 = xs5 == xs0

xs6 = removeElement xs0 0
ok23 = xs6 == [2, 3, 4]

xs7 = removeElement xs0 1
ok24 = xs7 == [1, 3, 4]

xs8 = removeElement xs0 3
ok25 = xs8 == [1, 2, 3]

xs9 = removeElement xs0 4
ok26 = xs9 == xs0


oks :: [Bool]
oks = [ok1, ok2, ok3, ok4, ok5, ok6, ok7, ok8, ok9, ok10, ok11, ok12
      , ok13, ok14, ok15, ok16, ok17, ok18, ok19, ok20, ok21, ok22
      , ok23, ok24, ok25, ok26
         ]

ok:: Bool
ok = and oks

--------------------------------------------------------------------------------

