# exclusive-choice

## To build and run:

Install [stack](https://docs.haskellstack.org/en/stable/README/)


     $ curl -sSL https://get.haskellstack.org/ | sh


Build and run

    $ stack build
    $ stack exec exclusive-choice
